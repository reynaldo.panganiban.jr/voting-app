Pod definition yamls are just for reference. We only use the deployment and service yamls

sample commands to create the deployment and services

```
kubectl create -f postgres-deployment.yaml
kubectl create -f postgres-service.yaml
kubectl create -f redis-deployment.yaml
kubectl create -f redis-service.yaml
kubectl create -f voting-app-deployment.yaml
kubectl create -f voting-app-service.yaml
kubectl create -f result-app-deployment.yaml
kubectl create -f result-app-service.yaml
kubectl create -f worker-deployment.yaml

kubectl get all -o wide

minikube service list -- to get the URLs of the services
|-------------|----------------|--------------|---------------------------|
|  NAMESPACE  |      NAME      | TARGET PORT  |            URL            |
|-------------|----------------|--------------|---------------------------|
| default     | db             | No node port |                           |
| default     | kubernetes     | No node port |                           |
| default     | redis          | No node port |                           |
| default     | result-service |           80 | http://192.168.49.2:30005 |
| default     | voting-service |           80 | http://192.168.49.2:30004 |
| kube-system | kube-dns       | No node port |                           |
|-------------|----------------|--------------|---------------------------|
```
